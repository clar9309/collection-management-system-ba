import Image from "next/image";
import Link from "next/link";

function DefaultHome() {
  return (
    <main className="pt-16 lg:h-screen lg:grid lg:items-center">
      <div className="grid lg:grid-cols-2">

        <div className="mt-10 lg:mt-0 grid items-center justify-center">
          <div>
            <h1 className="text-small lg:text-h1">
             Welcome to mercive
            </h1>
          </div>
        </div>
      </div>
    </main>
  );
}

export default DefaultHome;

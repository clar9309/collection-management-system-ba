import DefaultHome from "./DefaultHome";
import { getServerSession } from "next-auth";
import { redirect } from "next/navigation";

async function Home() {
  const session = await getServerSession();

  if (session) {
    redirect("/");
  }

  return !session && <DefaultHome />;
}
export default Home;

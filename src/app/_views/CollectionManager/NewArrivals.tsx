import React from 'react';
import ConfigContent from "@/app/_components/modules/ConfigContent";
import ExcludeOutOfStockProducts from "@/app/_components/modules/ExcludeOutOfStockProducts";
import UiButton from "@/app/_components/ui-library/UiButton";
import UiToggle from "@/app/_components/ui-library/UiToggle";
import SmartCollectionComponent from '@/app/_components/modules/SmartCollectionComponent';

export default function NewArrivals() {
    return (
      <div className="my-8">
        <div className="flex justify-between bg-gray-100 p-4">
        <h2 className="font-medium text-gray-500 uppercase ">New Arrivals</h2>
        <UiButton>Save</UiButton>
        </div>
        <div className=" border border-gray-100 p-4">
        <TagNewArrivals></TagNewArrivals>        
        </div>
        <div className=" border border-gray-100 p-4">
        <ExcludeOutOfStockProducts></ExcludeOutOfStockProducts>      
        </div>
        <div className="border border-gray-100 p-4">
        <SmartCollectionComponent></SmartCollectionComponent>  
        </div>
      </div>
    );
  }

  function TagNewArrivals() {
    return (
      <div className="">
      <ConfigContent 
      title={"Tag New Arrivals"} 
      description={"Automatically assign a specific tag to new (recently created) products. One tag only. Use this tag to create smart collections with new arrivals or exclude these products from smart collections. A maximum of 200 top products can be tagged."}> 
      <UiToggle></UiToggle>
      </ConfigContent>
      </div>
    )
  }
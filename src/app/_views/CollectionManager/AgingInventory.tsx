import React from 'react';

import ConfigContent from "@/app/_components/modules/ConfigContent";
import UiButton from "@/app/_components/ui-library/UiButton";
import UiToggle from "@/app/_components/ui-library/UiToggle";
import SmartCollectionComponent from '@/app/_components/modules/SmartCollectionComponent';

  export default function AgingInventory() {
    return (
      <div className="my-8">
        <div className="flex justify-between bg-gray-100 p-4">
        <h2 className="font-medium text-gray-500 uppercase ">Aging Inventory</h2>
        <UiButton>Save</UiButton>
        </div>
        <div className="border border-gray-100 p-4">
        <TagAgingInventory></TagAgingInventory>        
        </div>
        <div className="border border-gray-100 p-4">
        <SmartCollectionComponent></SmartCollectionComponent>  
        </div>
      </div>
    );
  }

  function TagAgingInventory() {
    return (
      <div className="">    
        <ConfigContent title={"Tag Aging Inventory"} description={"Automatically assign a specific tag to aging stock (all products that have no sales during the lookback period specified). Tags will be added to products with stock only. One tag only. Use this tag to create smart collections with aging stock products or exclude these products from smart collections."}>
        <div className="">
        <UiToggle></UiToggle>
          <div className="text-xs">
           Tag input here 
    </div>
        <div className="text-xs">
          Number of products stock to tag   
        </div>
        <div className="text-xs">
      Lookback period    
      </div>
    </div>    
        </ConfigContent>
      </div>  
    )
  }

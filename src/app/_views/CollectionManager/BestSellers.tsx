"use client"; 
import React from 'react';
import UiButton from "@/app/_components/ui-library/UiButton";
import UiToggle from "@/app/_components/ui-library/UiToggle";
import ConfigContent from '@/app/_components/modules/ConfigContent';
import BestSellerTagForm from '@/app/_components/forms/BestSellerTagForm';
import Link from 'next/link';
import Dropdown from '@/app/_components/ui-library/UiDropDown';
import LookBackPeriodForm from '@/app/_components/forms/LookBackPeriodForm';
import ExcludeOutOfStockProducts from '@/app/_components/modules/ExcludeOutOfStockProducts';
import SmartCollectionComponent from '@/app/_components/modules/SmartCollectionComponent';


  export default function BestSellers() {
    return (
      <div className="my-8">
        <div className="flex justify-between bg-gray-100 p-4">
        <h2 className="font-medium text-gray-500 uppercase ">Best Sellers</h2>
        <UiButton>Save</UiButton>
        </div>
        <div className=" border border-gray-100 p-4">
        <TagBestSeller></TagBestSeller>        
        </div>
        <div className=" border border-gray-100 p-4">
        <SelectBestSeller></SelectBestSeller>        
        </div>
        <div className="border border-gray-100 p-4">
        <ExcludeOutOfStockProducts></ExcludeOutOfStockProducts>      
        </div>
        <div className="border border-gray-100 p-4">
        <SmartCollectionComponent></SmartCollectionComponent>  
        </div>
      </div>
    );
  }


  function TagBestSeller() {
    return (
      <div>    
        <ConfigContent title={"Tag Bestsellers"} description={"Automatically assign a specific tag to top bestsellers. One tag only. Use this tag to create smart collections with bestsellers or exclude these products from smart collections. A maximum of 200 top products can be tagged."}>
        <div>
          <div className="mb-4">
    <UiToggle></UiToggle>
    </div>
    <div>
      <BestSellerTagForm>
      </BestSellerTagForm>
    </div>
    </div>    
    </ConfigContent>
      </div>  
    )
  }


  function SelectBestSeller() {
    return (
      <div>    
        <ConfigContent title={"Select Bestsellers By"} description={"Automatically assign a specific tag to top. Choose how the top bestsellers will be selected for tagging: by the number of sales or revenue. "}>
        <div className="flex flex-col ">
        <div className="mb-4">
    <UiToggle></UiToggle>
    </div>
          <div className="mb-4">
        <Dropdown buttonLabel="Select Bestsellers By">
        <Link href="#" className="text-gray-700 block px-4 py-2 text-sm" role="menuitem" tabIndex={-1} id="menu-item-0">By Number of Sales</Link>
        <Link href="#" className="text-gray-700 block px-4 py-2 text-sm" role="menuitem" tabIndex={-1} id="menu-item-1">By Sales Amount</Link>
        </Dropdown>
        </div>
        <LookBackPeriodForm></LookBackPeriodForm>

        </div>  
    </ConfigContent>
      </div>  
    )
  }



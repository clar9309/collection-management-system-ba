import React from 'react';

import ConfigContent from "@/app/_components/modules/ConfigContent";
import ExcludeOutOfStockProducts from "@/app/_components/modules/ExcludeOutOfStockProducts";
import UiButton from "@/app/_components/ui-library/UiButton";
import UiToggle from "@/app/_components/ui-library/UiToggle";
import SmartCollectionComponent from '@/app/_components/modules/SmartCollectionComponent';

  export default function Trending() {
    return (
      <div className="my-8">
        <div className="flex justify-between bg-gray-100 p-4">
        <h2 className="font-medium text-gray-500 uppercase ">Trending</h2>
        <UiButton>Save</UiButton>
        </div>
        <div className="border border-gray-100 p-4">
        <TagTrending></TagTrending>        
        </div>
        <div className="border border-gray-100 p-4">
       <ExcludeOutOfStockProducts></ExcludeOutOfStockProducts>      
        </div>
        <div className="border border-gray-100 p-4">
        <SmartCollectionComponent></SmartCollectionComponent>  
        </div>
      </div>
    );
  }

  function TagTrending() {
    return (
      <div className="">    
        <ConfigContent title={"Tag Trending"} description={"Automatically assign a specific tag to top trending products (the store's last bought and the most popular products among customers). Shorter lookback period is recommended. One tag only."}>
       <UiToggle></UiToggle>
        </ConfigContent>
      </div>  
    )
  }
import React from 'react';

import UiButton from "@/app/_components/ui-library/UiButton";
import ExcludeOutOfStockProducts from '@/app/_components/modules/ExcludeOutOfStockProducts';


  export default function AdditionalSettings() {
    return (
      <div className="my-8">
        <div className="flex justify-between bg-gray-100 p-4">
        <h2 className="font-medium text-gray-500 uppercase ">Additional Settings</h2>
        <UiButton>Save</UiButton>
        </div>
        <div className="border border-gray-100 p-4">
        <ExcludeOutOfStockProducts></ExcludeOutOfStockProducts>      

        </div>
      </div>
    );
  }



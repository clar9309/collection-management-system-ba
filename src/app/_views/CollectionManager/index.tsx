import React from 'react';

import AdditionalSettings from "./AdditionalSettings";
import AgingInventory from "./AgingInventory";
import BestSellers from "./BestSellers";
import NewArrivals from "./NewArrivals";
import Trending from "./Trending";

export default function CollectionManager() {
  return (
    <div className="ml-12">
      <div className="px-12 mt-6">
      <h1 className="text-2xl font-bold">Collection Manager</h1>
        <div className="">
        <BestSellers></BestSellers>
        <Trending></Trending>
        <NewArrivals></NewArrivals>
        <AgingInventory></AgingInventory>
        <AdditionalSettings></AdditionalSettings>
       </div>
    </div>
    </div>
  );
}

import UiButton from "@/app/_components/ui-library/UiButton";
import Link from "next/link";

export default function FeaturedProducts() {
    return (
      <div className="my-8">
        <div className="flex justify-between bg-gray-100 p-4">
        <h2 className="font-medium text-gray-500 uppercase">Featured Products</h2>
        <UiButton >Save</UiButton>
        </div>
        <div className="flex justify-between items-center border border-gray-100 p-4">
         
          <div className="flex-1 ">
            <div className="flex justify-between">
          <h2 className="underline mb-4">Featured Products</h2>
          <div className=""><Search></Search></div>
          </div>
            <p className="max-w-[30%] ">Move products up/down in the list by dragging them. Schedule products by choosing a start date and the number of days when the product will be featured. At the end of this period a product will be removed from featured automatically.</p>
          </div>
        </div>
      </div>
    );
  }
  
function Search() {
    return (
      <input
      type="text"
      placeholder="Search for products ..."
      className="flex p-2 border rounded w-[400px]"
    />
    )
  }
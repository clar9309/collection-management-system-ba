import AutomatedSettings from "./AutomatedSettings";
import FeaturedProducts from "./FeaturedProducts";
import CollectionSettings from "./CollectionSettings";


export default function CustomizeCollection() {

  return (
    <div className="ml-12">
      <div className="px-12 mt-6">
        <h1 className="text-2xl font-bold">Collection name</h1>
        <div className="">
          <AutomatedSettings></AutomatedSettings>
          <FeaturedProducts></FeaturedProducts>
          <CollectionSettings></CollectionSettings>
        </div>
      </div>
    </div>
  );
}

import Link from "next/link";

export default function AutomatedSettings() {
    return (
      <div className="my-8">
        <div className="flex justify-between  bg-gray-100 p-4">
        <h2 className="font-medium text-gray-500 uppercase">Collection Settings</h2>
        <Link href={"/dashboard"}>Back to collections</Link>
        </div>
        <div className="flex justify-between items-center border border-gray-100 p-4">
          <div className="flex-1 max-w-[30%]">
            <p>Customize sorting rules for this collection specifically. Select products to be featured in this collection and specify order for these products.
            These products will always stay at the top of this collection irrespective of the other rules configured globally.</p>
          </div>
        </div>
      </div>
    );
  }
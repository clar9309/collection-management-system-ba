"use client"
import React from 'react';
import { useState, useRef, useEffect } from 'react';
import UiButton from "@/app/_components/ui-library/UiButton";
import UiToggle from '@/app/_components/ui-library/UiToggle';
import StoreUs from '../../data/store_us.json';
import StoreUk from '../../data/store_uk.json';

export default function CollectionSettings() {
    return (
      <div className="my-8">
        <div className="flex justify-between bg-gray-100 p-4">
        <h2 className="font-medium text-gray-500 uppercase">Collection Settings</h2>
        <UiButton>Save</UiButton>
        </div>
        <div className="flex justify-between items-center border border-gray-100 p-4">
        <EnableSortingRules></EnableSortingRules>
        </div>
        <div className="flex justify-between items-center border border-gray-100 p-4">
        <SortOrder></SortOrder>
        </div>
        <div className="flex justify-between items-center border border-gray-100 p-4">
        <ImportExportProduct></ImportExportProduct>
        </div>
        <div className="flex justify-between items-center border border-gray-100 p-4">
        <LookBackPeriod></LookBackPeriod>
        </div>
        <div className="flex justify-between items-center border border-gray-100 p-4">
        <OrderStatus></OrderStatus>
        </div>
        <div className="flex justify-between items-center border border-gray-100 p-4">
        <IncludeDiscount></IncludeDiscount>
        </div>
        <div className="flex justify-between items-center border border-gray-100 p-4">
        <PushNewProducts></PushNewProducts>
        </div>
        <div className="flex justify-between items-center border border-gray-100 p-4">
        <PushDownOutOfStock></PushDownOutOfStock>
        </div>
        <div className="font-bold">
        *Missing here*
        - Out of stock vs. New 
        - Out of stock cs featured
        - Out of stock vs. tags *
        </div>
        
      </div>
    );
  }

function EnableSortingRules() {
  return (
    <div className="flex-1 ">
            <div className="flex justify-between">
          <h2 className="underline mb-4">Use Custom Sorting Rules </h2>
          <div className="">
          <UiToggle></UiToggle>
          </div>
          </div>
            <p className="md:max-w-[30%] ">Enable to override global sorting rules and specify a number of options for this collection only.</p>
          </div>
          )
}

function SortOrder() {
  return (
    <div className="flex-1 ">
            <div className="flex justify-between">
          <h2 className="underline mb-4">Primary Sort Order</h2>
          <div className="">
          <SortDropDown></SortDropDown>
          </div>
          </div>
            <p className="md:max-w-[30%]">Default sorting rule for ordering products in a collection. Applied to automated and manual sorting.</p>
          </div>
          )
}

const sorts = {
  'revenue': 'Revenue Generated - High to Low',
  'sales': 'Number of Sales - High to Low',
  'newest': 'Date - Newest to Oldest',
  'oldest': 'Date - Oldest to Newest',
  'inventoryHigh': 'Inventory Qty - High to Low',
  'inventoryLow': 'Inventory Qty - Low to High',
  'price': 'Price',
};

function SortDropDown() {
  const [isOpen, setIsOpen] = useState(false);
  const dropdownRef = useRef(null);
  const firstOption = Object.values(sorts)[0];

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  useEffect(() => {
    function handleClickOutside(event) {
      if (
        dropdownRef.current &&
        !dropdownRef.current.contains(event.target) &&
        !event.target.closest('#menu-button')
      ) {
        setIsOpen(false);
      }
    }

    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [dropdownRef]);

  return (
    <div className="relative inline-block text-left" ref={dropdownRef}>
      <div>
        <UiButton
          className="inline-flex w-full justify-center gap-x-1.5 rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50"
          id="menu-button"
          aria-expanded={isOpen}
          aria-haspopup="true"
          onClick={toggleDropdown}
        >
          {firstOption} 
          <svg
            className={`-mr-1 h-5 w-5 text-gray-400 transition-transform transform ${
              isOpen ? 'rotate-180' : ''
            }`}
            viewBox="0 0 20 20"
            fill="currentColor"
            aria-hidden="true"
          >
            <path
              fillRule="evenodd"
              d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z"
              clipRule="evenodd"
            />
          </svg>
        </UiButton>
      </div>

      {isOpen && (
        <div
          className="absolute left-auto right-0 z-10 mt-2 w-56 origin-top-right rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none"
          role="menu"
          aria-orientation="vertical"
          aria-labelledby="menu-button"
          tabIndex="-1"
        >
          <div className="py-1" role="none">
            {Object.entries(sorts).map(([key, value], index) => (
              <a
                key={index}
                href="#"
                className="text-gray-700 block px-4 py-2 text-sm"
                role="menuitem"
                tabIndex="-1"
                id={`menu-item-${index}`}
              >
                {value}
              </a>
            ))}
          </div>
        </div>
      )}
    </div>
  );
}

function ImportExportProduct() {
  // State to store the imported file
  const [importedFile, setImportedFile] = useState(null);

  // Function to handle export action
  const handleExport = () => {
    const dataToExport = "This is mock data to export";
    const exportedFilename = "collection_products.csv";

// Her skal data erstattes    
  const blob = new Blob([dataToExport], { type: 'text/csv' });

    // Temporary link to download the files
    const url = window.URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.download = exportedFilename;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    window.URL.revokeObjectURL(url);
  };

  // Function to handle import
  const handleImport = (event) => {
    const file = event.target.files[0];
    if (file) {
      setImportedFile(file);

      const reader = new FileReader();
      reader.onload = (e) => {
        const importedData = e.target.result;
        console.log("Imported data:", importedData);
      };
      reader.readAsText(file);
    }
  };

  return (
    <div className="flex-1 ">
      <div className="flex justify-between">
        <h2 className="underline mb-4">Import/Export Products Order</h2>
        <div className="flex gap-2 items-center">
          <UiButton onClick={handleExport}>Export</UiButton>
          <label htmlFor="import-input" className="cursor-pointer">
            <UiButton as="span">Import</UiButton>
            <input
              id="import-input"
              type="file"
              accept=".csv"
              style={{ display: 'none' }}
              onChange={handleImport}
            />
          </label>
        </div>
      </div>
      <p className="md:max-w-[30%] ">You can import rearranged products. Click "Export" to download your current collection products. Rearrange products in external software and import back.</p>
    </div>
  );
}

function LookBackPeriod() {
  const [days, setDays] = useState(""); 

  const handleChange = (event) => {
    const value = event.target.value;
    if (/^\d+$/.test(value) && parseInt(value) <= 365) {
      setDays(value);
    }
  };

  return (
    <div className="flex-1">
      <div className="flex justify-between">
        <h2 className="underline mb-4">Lookback Period</h2>
        <div className="flex gap-2 items-center">
          <input
            type="number"
            className="w-20 px-2 py-1 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-blue-200"
            value={days}
            onChange={handleChange}
            placeholder="180"
            min="1"
            max="365"
          />
          <p>Days</p>
        </div>
      </div>
      <p className="md:max-w-[30%]">How many days to look back to analyze. Used for sorting by revenue, by the number of sales. 365 days maximum</p>
    </div>
  );
}

const order = {
  "all_orders": "All Orders",
  "paid_orders": "Paid Orders Only",

}

function OrderStatus() {
  return (
    <div className="flex-1">
      <div className="flex justify-between">
        <h2 className="underline mb-4">Order Status</h2>
        <div className="flex gap-2 items-center">
        <OrderDropDown></OrderDropDown>
        </div>
      </div>
      <p className="md:max-w-[30%]">Orders to be used for revenue and sales calculations.</p>
    </div>
  );
}

function OrderDropDown() {
  const [isOpen, setIsOpen] = useState(false);
  const dropdownRef = useRef(null);
  const firstOption = Object.values(order)[0];

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  useEffect(() => {
    function handleClickOutside(event) {
      if (
        dropdownRef.current &&
        !dropdownRef.current.contains(event.target) &&
        !event.target.closest('#menu-button')
      ) {
        setIsOpen(false);
      }
    }

    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [dropdownRef]);

  return (
    <div className="relative inline-block text-left" ref={dropdownRef}>
      <div>
        <UiButton
          className="inline-flex w-full justify-center gap-x-1.5 rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50"
          id="menu-button"
          aria-expanded={isOpen}
          aria-haspopup="true"
          onClick={toggleDropdown}
        >
          {firstOption} 
          <svg
            className={`-mr-1 h-5 w-5 text-gray-400 transition-transform transform ${
              isOpen ? 'rotate-180' : ''
            }`}
            viewBox="0 0 20 20"
            fill="currentColor"
            aria-hidden="true"
          >
            <path
              fillRule="evenodd"
              d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z"
              clipRule="evenodd"
            />
          </svg>
        </UiButton>
      </div>

      {isOpen && (
        <div
          className="absolute left-auto right-0 z-10 mt-2 w-56 origin-top-right rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none"
          role="menu"
          aria-orientation="vertical"
          aria-labelledby="menu-button"
          tabIndex="-1"
        >
          <div className="py-1" role="none">
            {Object.entries(order).map(([key, value], index) => (
              <a
                key={index}
                href="#"
                className="text-gray-700 block px-4 py-2 text-sm"
                role="menuitem"
                tabIndex="-1"
                id={`menu-item-${index}`}
              >
                {value}
              </a>
            ))}
          </div>
        </div>
      )}
    </div>
  );
}

function IncludeDiscount() {
  return (
    <div className="flex-1 ">
            <div className="flex justify-between">
          <h2 className="underline mb-4">Include Discounts</h2>
          <div className="">
          <label className="inline-flex items-center cursor-pointer">
            <input 
              type="checkbox" 
              className="sr-only peer"
            />
            <div className="relative w-11 h-6 bg-gray-200 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
          </label>
          </div>
          </div>
            <p className="md:max-w-[30%] ">Ignore or include discounts in revenue calculation. When disabled, discounts are ignored and revenue is calculated as (price x number of items). When enabled, discounts are included in the calculation and revenue is equal to (price - discount) x number of items.</p>
          </div>
          )
}

function PushNewProducts() {
  const [days, setDays] = useState(""); 

  const handleChange = (event) => {
    const value = event.target.value;
    if (/^\d+$/.test(value) && parseInt(value) <= 365) {
      setDays(value);
    }
  };
  return (
    <div className="flex-1 ">
            <div className="flex justify-between">
              <div className="">
          <h2 className="underline mb-4">Push New Products Up</h2>
          <p className="md:max-w-[30%] ">Automatically push new products to the top of a collection. Set the number of days in the collection for the product to be considered new.</p>
          </div>
          <div className="">
          <label className="inline-flex  items-center cursor-pointer">
            <input 
              type="checkbox" 
              className="sr-only peer"
            />
            <div className="relative w-11 h-6 bg-gray-200 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
          </label>
          <div className="flex items-center gap-2 ">
          <input
            type="number"
            className="w-20 px-2 py-1 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-blue-200"
            value={days}
            onChange={handleChange}
            placeholder="180"
            min="1"
            max="365"
          />
          <p>Days</p>
          </div>
          </div>
          </div>

          </div>
          )
}

function PushDownOutOfStock() {

  return (
    <div className="flex-1 ">
            <div className="flex justify-between">
              <div className="">
          <h2 className="underline mb-4">Push New Products Up</h2>
          </div>
          <div className="">
          <label className="inline-flex items-center cursor-pointer">
            <input 
              type="checkbox" 
              className="sr-only peer"
            />
            <div className="relative w-11 h-6 bg-gray-200 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
          </label>
          
          </div>
          </div>
          <p className="md:max-w-[30%] ">Automatically push out-of-stock products to the bottom of a collection</p>
          </div>
          )
}


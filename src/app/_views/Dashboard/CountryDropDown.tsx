import UiButton from "@/app/_components/ui-library/UiButton";
import { useEffect, useRef, useState } from "react";

export default function CountryDropDown({ onChange }: { onChange: (value: string) => void }){
    const [isOpen, setIsOpen] = useState(false);
    const [selectedOption, setSelectedOption] = useState('USA');
    const dropdownRef = useRef(null);
  
    const toggleDropdown = () => {
      setIsOpen(!isOpen);
    };
  
    const handleOptionSelect = (option) => {
      let formattedOption = '';
      if (option === 'store_us') {
        formattedOption = 'USA';
      } else if (option === 'store_uk') {
        formattedOption = 'United Kingdom';
      }
      setSelectedOption(formattedOption);
      onChange(option); 
      setIsOpen(false); 
    };
  
    useEffect(() => {
      function handleClickOutside(event) {
        if (
          dropdownRef.current &&
          !dropdownRef.current.contains(event.target) &&
          !event.target.closest('#store-select-button')
        ) {
          setIsOpen(false);
        }
      }
    
      document.addEventListener('mousedown', handleClickOutside);
      return () => {
        document.removeEventListener('mousedown', handleClickOutside);
      };
    }, [dropdownRef]);
  
    return (
      <div className="relative inline-block text-left" ref={dropdownRef}>
        <div>
          <UiButton
            type="button"
            className="inline-flex w-full justify-center gap-x-1.5 rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50"
            id="store-select-button"
            aria-expanded={isOpen}
            aria-haspopup="true"
            onClick={toggleDropdown}
          >
            {selectedOption}
            <svg
              className={`-mr-1 h-5 w-5 text-gray-400 transition-transform transform ${
                isOpen ? 'rotate-180' : ''
              }`}
              viewBox="0 0 20 20"
              fill="currentColor"
              aria-hidden="true"
            >
              <path
                fillRule="evenodd"
                d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z"
                clipRule="evenodd"
              />
            </svg>
          </UiButton>
        </div>
  
        {isOpen && (
          <div
            className="absolute left-0 z-10 mt-2 w-56 origin-top-left rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none"
            role="menu"
            aria-orientation="vertical"
            aria-labelledby="store-select-button"
            tabIndex="-1"
          >
            <div className="py-1" role="none">
              <div
                className="text-gray-700 block px-4 py-2 text-sm cursor-pointer hover:bg-gray-100"
                onClick={() => handleOptionSelect('store_us')}
              >
                USA
              </div>
              <div
                className="text-gray-700 block px-4 py-2 text-sm cursor-pointer hover:bg-gray-100"
                onClick={() => handleOptionSelect('store_uk')}
              >
                United Kingdom
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
  
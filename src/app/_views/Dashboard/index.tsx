"use client"; 
import React, {Fragment, useState, useEffect, useRef } from 'react';
import StoreUS from '../../data/store_us.json';
import StoreUK from '../../data/store_uk.json';
import UiButton from '@/app/_components/ui-library/UiButton';
import Table from './Table';
import { useLocalStorage } from 'usehooks-ts';
import FilterDropDown from './FilterDropDown';
import CountryDropDown from './CountryDropDown';
import SearchInput from './Search';


export default function Dashboard() {
  const [store, setStore] = useLocalStorage('store', 'store_us');
  const [collections, setCollections] = useLocalStorage('collections', []); 
  const [searchQuery, setSearchQuery] = useState('');
  const [selectAll, setSelectAll] = useState(false);
  const [checkedCount, setCheckedCount] = useState(0);

  useEffect(() => {
    const loadedCollections = store === 'store_us' ? StoreUS.collections : StoreUK.collections;
    setCollections(loadedCollections.map((collection) => ({
      ...collection,
      enabled: selectAll,
      toggleStatus: 'enabled'
    })));
  }, [store, selectAll]);

  const toggleCheckBox = (index) => {
    const updatedCollections = [...collections];
    updatedCollections[index].enabled = !updatedCollections[index].enabled;
    setCollections(updatedCollections);

    const checkedCollectionsCount = updatedCollections.filter(collection => collection.enabled).length;
    setCheckedCount(checkedCollectionsCount);
  };

  const toggleSelectAll = () => {
    setSelectAll(!selectAll);
  };

  const toggleStatus = (index) => {
    const updatedCollections = [...collections];
    updatedCollections[index].toggleStatus = updatedCollections[index].toggleStatus === 'enabled' ? 'disabled' : 'enabled';
    setCollections(updatedCollections);
  };

  useEffect(() => {
    setCheckedCount(collections.filter(collection => collection.enabled).length);
  }, [collections]);

  const searchProducts = () => {
    if (!searchQuery) return collections;

    const lowerQuery = searchQuery.toLowerCase();
    return collections.filter(collection => {
      const matchesName = collection.name.toLowerCase().includes(lowerQuery);
      const matchesDescription = collection.description.toLowerCase().includes(lowerQuery);
      const matchesTags = collection.tags ? collection.tags.some(tag => tag.toLowerCase().includes(lowerQuery)) : false;

      return matchesName || matchesDescription || matchesTags;
    });
  };

  const filteredCollections = searchProducts();

  return (
    <div className="ml-12">
      <div className="px-12 mt-6">
        <h1 className="text-2xl font-semibold mb-8">Dashboard</h1>
        <div className="">
          <div className="flex justify-between mb-4">
            <div className="flex gap-8 items-center">
              <p>Store:</p>
              <CountryDropDown onChange={(value) => setStore(value)} />
            </div>
          </div>
          <div className="flex flex-col md:flex-row items-center justify-between">
            <div className="flex gap-6">
              <UiButton onClick={() => setStore('all')} className="mr-2">All ({collections.length})</UiButton>
              <FilterDropDown></FilterDropDown>
            </div>
            <SearchInput value={searchQuery} onChange={(e) => setSearchQuery(e.target.value)} />
          </div>
        </div>
        {filteredCollections.length === 0 ? (
          <p className="text-gray-600 mt-4">No collection found</p>
        ) : (
          <Table
            collections={filteredCollections}
            selectAll={selectAll}
            toggleSelectAll={toggleSelectAll}
            toggleCheckBox={toggleCheckBox}
            toggleStatus={toggleStatus}
          />
        )}
      </div>
    </div>
  );
}
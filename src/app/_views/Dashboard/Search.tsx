export default function SearchInput(
  { 
    value, 
    onChange 
  }: 
  { 
    value: string; 
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void 
  }) {

  return (
    <input
      type="text"
      placeholder="Search ..."
      value={value}
      onChange={onChange}
      className="p-2 border rounded"
    />
  );
}

"use client"
import Link from 'next/link';
import React, { useState, useEffect } from 'react';

export default function Table(
    {
    collections, 
    selectAll, 
    toggleSelectAll, 
    toggleCheckBox, 
    toggleStatus 
  }: 
    { 
      collections: any[]; 
      selectAll: boolean; 
      toggleSelectAll: () => void; 
      toggleCheckBox: (index: number) => void; 
      toggleStatus: (index: number) => void}) {
    return (
      <div className="bg-white rounded-lg mt-4 overflow-x-auto">
        <table className="min-w-full">
          <thead className="bg-gray-100">
            <tr>
              <th className="text-left font-medium text-gray-500 uppercase tracking-wider pl-4 ">
                <input 
                  type="checkbox" 
                  checked={selectAll} 
                  onChange={toggleSelectAll} 
                />
              </th>
              <th className="px-6 py-3 text-left font-medium text-gray-500 uppercase tracking-wider">Status</th>
              <th className="px-6 py-3 text-left font-medium text-gray-500 uppercase tracking-wider">Type</th>
              <th className="px-6 py-3 text-left font-medium text-gray-500 uppercase tracking-wider">Collection</th>
              <th className="px-6 py-3 text-left font-medium text-gray-500 uppercase tracking-wider">Custom</th>
            </tr>
          </thead>
          <tbody className="border border-gray-100">
            {collections.map((collection, index) => (
              <TableRow
                key={collection._id}
                collection={collection}
                index={index}
                toggleCheckBox={(index) => toggleCheckBox(index)}
                toggleStatus={(index) => toggleStatus(index)}
              />
            ))}
          </tbody>
        </table>
      </div>
    );
  }


export function TableRow(
    { 
      collection, 
      index, 
      toggleCheckBox, 
      toggleStatus 
    }: 
    { 
      collection: any; 
      index: number; 
      toggleCheckBox: (index: number) => void; 
      toggleStatus: (index: number) => void }) {
  
    const [showButtons, setShowButtons] = useState(false);
    const handleCheckboxChange = () => {
      toggleCheckBox(index);
      setShowButtons(!showButtons);
    };
  
    return (
      <tr className="border-b border-gray-100" onMouseEnter={() => setShowButtons(true)} onMouseLeave={() => setShowButtons(false)}>
        <td className="whitespace-nowrap pl-4">
          <input 
            type="checkbox" 
            checked={collection.enabled} 
            onChange={handleCheckboxChange}
          />
        </td>
        <td className="px-6 py-4 whitespace-nowrap">
          <label className="inline-flex items-center cursor-pointer">
            <input 
              type="checkbox" 
              className="sr-only peer"
              checked={collection.toggleStatus === 'enabled'}
              onChange={() => toggleStatus(index)}
            />
            <div className="relative w-11 h-6 bg-gray-200 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
          </label>
        </td>
        <td className="px-6 py-4 whitespace-nowrap"> <span className='bg-lightGrey p-2 text-sm rounded-lg text-darkGrey'>{collection.type}</span> </td>
        <td className="px-6 py-4 whitespace-nowrap">{collection.name}</td>
        <td className="px-6 py-4 whitespace-nowrap">
        <td className="px-6 py-4 whitespace-nowrap">
  {collection._id ? (
    <Link href={`/customize-collection/${collection._id}`}>
      Customize collection
    </Link>
  ) : (
    <span>Invalid Collection ID</span>
  )}
</td>
        </td>
      </tr>
    );
  }
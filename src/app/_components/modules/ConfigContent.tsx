import React, { ReactNode, FC } from 'react';

interface ConfigContentProps {
  title: string;
  description: string;
  children?: ReactNode;
}

const ConfigContent: FC<ConfigContentProps> = ({ title, description, children }) => {
  return (
    <div className="w-full">
      <div className="grid grid-cols-1 md:grid-cols-4 gap-4">
        <div className="md:col-span-3">
          <h2 className="underline mb-2 text-sm font-semibold">{title}</h2>
          <p className="text-sm max-w-[50%]">{description}</p>
        </div>
        <div className="md:col-span-1 flex justify-between">
          {children}
        </div>
      </div>
    </div>
  );
};

export default ConfigContent;

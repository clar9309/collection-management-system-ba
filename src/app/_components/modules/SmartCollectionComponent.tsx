import React from 'react';
import UiToggle from "../ui-library/UiToggle";
import ConfigContent from "./ConfigContent";
import SmartCollectionForm from '../forms/SmartCollectionForm';

function SmartCollectionComponent() {
  return (
    <div>    
      <ConfigContent 
        title="Create Smart Collection" 
        description="The app automatically creates a smart collection using tagged bestsellers. All tagged bestsellers will be included. Products that lose their tag will be excluded automatically."
      >
        <UiToggle /> 
      </ConfigContent>
    </div>  
  );
}

export default SmartCollectionComponent;

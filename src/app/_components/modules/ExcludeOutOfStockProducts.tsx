import React from 'react';

import UiToggle from "../ui-library/UiToggle";
import ConfigContent from "./ConfigContent";

function ExcludeOutOfStockProducts() {
    return (
      <div>    
        <ConfigContent title={"Exclude Out-of-Stock products"} description={"Enable to exclude out-of-stock best-selling products from tagging. "}>
        <UiToggle></UiToggle>
    </ConfigContent>
      </div>  
    )
  }
  export default ExcludeOutOfStockProducts; 
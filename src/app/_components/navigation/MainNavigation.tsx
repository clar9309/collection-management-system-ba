import Link from "next/link";

async function MainNavigation() {

  return (
    <>
        <>
          <Link href="/dashboard" className="md:hidden pt-9 px-4 ">
          <Logo></Logo>
          </Link>
          <div className="h-full w-full md:auto md:h-screen bg-gray-100 ">
            <nav className="fixed bottom-0 left-0 w-screen md:w-auto md:h-full z-20 bg-gray-100">
              <ul className="flex w-full justify-evenly items-center md:justify-start pb-3 pt-2 md:pt-7 md:w-auto md:flex-col md:items-start md:gap-6 md:px-9 md:h-full">
              <li>
        <Link href="/dashboard" className="p-2">
            Dashboard
        </Link>
      </li>
      <li>
        <Link href="/settings"  className="p-2">
            Settings
        </Link>
      </li>
      <li>
        <Link href="/collection-manager" className="p-2">
            Collection
        </Link>
      </li>
      <li>
        <Link href="/reports" className="p-2 ">
     
          Reports
        </Link>
      </li>
      <li>
        <Link href="/statistics" className="p-2">
        Statistics
        </Link>
      </li>
      <li>
        <Link href="/resort" className="p-2">
            Re-Sort
        </Link>
      </li>
             
              </ul>
            </nav>
          </div>
        </>
    </>
  );
}

export default MainNavigation;


export function Logo() {
  return (
    <Link href={'/'}>
         <svg width="100" height="" viewBox="0 0 365 80" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M59.638 63.7988V20.3515L45.6995 63.7988H34.1409L20.2024 20.3515V63.7988H12.7229V16.2041H26.6614L39.9202 58.0195L53.179 16.2041H67.1175V63.7988H59.638Z" fill="black"/>
<path d="M111.591 16.2041V23.3428H84.3938V35.5821H109.551V42.7208H84.3938V56.6593H112.271V63.7989H76.9144V16.2041H111.591Z" fill="black"/>
<path d="M149.808 63.7988C149.196 62.6426 148.787 61.2832 148.516 59.8554C148.243 58.3593 148.108 56.0478 148.108 52.9199C148.108 46.3242 145.117 44.081 139.609 44.081H128.05V63.7988H120.571V16.2041H143.348C152.935 16.2041 158.647 21.8467 158.647 29.1221C158.647 35.1739 155.111 39.5928 149.128 41.7012C153.751 43.5371 156.335 46.9366 156.607 52.9883C156.743 55.7754 156.946 57.8154 157.151 59.1748C157.423 60.4668 158.034 61.6914 158.986 62.7109V63.7988H149.808ZM142.668 36.9414C147.836 36.9414 150.828 34.4941 150.828 30.1426C150.828 25.7911 147.836 23.3428 142.668 23.3428H128.05V36.9414H142.668Z" fill="black"/>
<path d="M188.432 57.6796C193.396 57.6796 197.339 56.2519 200.263 53.3954C203.187 50.5399 205.023 47.0048 205.771 42.7206L212.57 45.4403C210.122 56.4559 201.487 64.4784 188.432 64.4784C174.085 64.6825 164.159 53.8036 164.294 40.0009C164.023 26.3349 174.902 15.2519 188.432 15.5234C200.534 15.5234 207.674 22.9345 209.85 31.5019L203.05 34.5615C201.622 27.4228 196.523 22.3232 188.432 22.3232C179.185 22.3232 172.114 29.3261 172.114 40.0009C172.114 51.0839 178.777 57.6796 188.432 57.6796Z" fill="black"/>
<path d="M219.646 16.2041H227.125V63.7988H219.646V16.2041Z" fill="black"/>
<path d="M273.77 16.2041H282.201L262.618 63.7988H252.283L232.702 16.2041H241.133L257.452 57.7471L273.77 16.2041Z" fill="black"/>
<path d="M322.457 16.2041V23.3428H295.26V35.5821H320.417V42.7208H295.26V56.6593H323.137V63.7989H287.781V16.2041H322.457Z" fill="black"/>
<path d="M338.702 16.2041H341.045L336.783 29.6443H332.174L327.912 16.2041H330.254L334.478 29.5292L338.702 16.2041Z" fill="black"/>
<path d="M341.487 29.6443H339.144L343.406 16.2041H348.014L352.277 29.6443H349.935L345.711 16.3192L341.487 29.6443Z" fill="black"/>
</svg>

    </Link>
  );
}

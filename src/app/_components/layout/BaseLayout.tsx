import MainNavigation from "../navigation/MainNavigation";

function BaseLayout(props: { children: React.ReactNode }) {
  return (
    <div className="text-black min-h-screen max-w-screen relative overflow-hidden">
      <div
        className={`grid relative md:relative z-20 md:min-h-screen ${
          props && "md:grid-cols-[8.125rem,1fr]"
        }`}
      >
        <MainNavigation />
        <div className="w-full px-6 pb-24 md:pb-0 overflow-y-auto">
          {props.children}
        </div>
      </div>
      <div className="absolute inset-0 -z-20"></div>
    </div>
  );
}

export default BaseLayout;

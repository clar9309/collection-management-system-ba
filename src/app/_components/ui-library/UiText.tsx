import React from 'react';

interface UiTextProps {
    as?: React.ElementType;
    children: React.ReactNode;
    className?: string;
}

const UiText: React.FC<UiTextProps> = ({ as: Component = 'p', children, className = '', ...props }) => {
    return (
        <Component className={`ui-text ${className}`} {...props}>
            {children}
        </Component>
    );
};

export default UiText;

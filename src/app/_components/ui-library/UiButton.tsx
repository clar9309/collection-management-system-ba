import React, { ReactNode } from 'react';

export default function UiButton({ children, onClick, className, ...props }: { children?: ReactNode, onClick?: () => void, className?: string, props?: any }) {
    return (
        <button
            className={`ui-button ${className}`}
            onClick={onClick}
            {...props}
        >
            {children}
        </button>
    );
}
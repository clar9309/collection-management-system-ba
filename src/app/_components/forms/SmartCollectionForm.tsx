import React from 'react';
import { Formik, Form, Field } from 'formik';
import SmartCollectionInput from './formInputs/SmartCollectionInput';
import createsmartcollectionschema from '@/app/_utils/schemas/create-smart-collection-schema';

function SmartCollectionForm() {
  return (
    <div className="text-xs">
      <Formik
        initialValues={{
          collectionName: ''
        }}
        validationSchema={createsmartcollectionschema}
        onSubmit={(values, actions) => {
          console.log('Form Values:', values);
          actions.setSubmitting(false);
        }}
      >
        {({ errors, touched }) => (
          <Form>
            <Field name="collectionName" component={SmartCollectionInput} />
            <button type="submit" className="mt-4 bg-blue-500 text-white px-4 py-2 rounded">
              Submit
            </button>
          </Form>
        )}
      </Formik>
    </div>
  );
}

export default SmartCollectionForm;

import React from 'react';

import { Field, ErrorMessage } from "formik";
import { minTagNumber, maxTagNumber } from "@/app/_utils/validation/validationVariables";
import UiText from '../../ui-library/UiText';

function TagNumberInput({
  error,
  touched,
}: {
  error: string | undefined;
  touched: boolean | undefined;
}) {
  return (
    <div className="">
      <UiText className="text-xs font-medium">Number of top bestsellers to tag</UiText>
      <label hidden htmlFor="number">Number of Bestsellers to Tag</label>
      <Field
        type="number"
        name="tagnumber"
        id="number"
        min={minTagNumber}
        max={maxTagNumber}
        className={`border border-lightGrey p-2 rounded-lg focus:outline-none focus:border-darkGrey focus:border mt-2 
           ${error && touched ? "border border-warning" : ""}`}
        placeholder="50"
      />

      <ErrorMessage
        className="text-warning mt-2 italic text-right text-sm"
        name="tagnumber"
        component="div"
      />
    </div>
  );
}

export default TagNumberInput;

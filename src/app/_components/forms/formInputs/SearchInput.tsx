import React from 'react';

import { Field, ErrorMessage, FormikErrors } from "formik";

function SearchInput({
  error,
  touched,
  children,
  setFieldValue,
  setErrors,
}: {
  error: string | undefined;
  touched: boolean | undefined;
  children: React.ReactNode;
  setFieldValue: (
    field: any,
    value: any,
    validate?: boolean
  ) => Promise<void | FormikErrors<{ search: string }>>;
  setErrors: any;
}) {
  return (
    <div className="min-h-[5.3rem] flex flex-col w-full">
      <label
        htmlFor="search"
        hidden
        className="font-medium text-h5 text-darkGrey mb-1"
      >
        Search for collections
      </label>
      <div className="flex gap-2 w-full">
        <Field
          type="text"
          name="search"
          id="search"
          className={`bg-primary  w-full text-white h-14 placeholder:text-darkGrey focus:outline-none focus:border-secondary focus:border px-5 rounded-l-lg 
           ${error && touched && "border border-warning"}`}
          placeholder="Collection"
        />
        {children}
      </div>
      <ErrorMessage
        className="text-warning mt-2 italic text-right text-sm"
        name="search"
        component="div"
      />
    </div>
  );
}

export default SearchInput;

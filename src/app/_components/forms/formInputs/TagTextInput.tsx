import React from 'react';

import { maxTagText, minTagText } from "@/app/_utils/validation/validationVariables";
import { Field, ErrorMessage } from "formik";
import UiText from '../../ui-library/UiText';


function TagTextInput({
  error,
  touched,
}: {
  error: string | undefined;
  touched: boolean | undefined;
}) {
  return (
    <div className="">
      <label hidden htmlFor="text"></label>
      <Field
        type="text"
        name="text"
        id="text"
        min={minTagText}
        max={maxTagText}
        className={`border border-lightGrey p-2 rounded-lg focus:outline-none focus:border-darkGrey focus:border  
           ${error && touched && "border border-warning"}`}
        placeholder={"bestseller-resort"}
      />
      <UiText className="text-xs text-darkGrey py-2">Use letter, numbers, dashes or underscores only</UiText>
      <ErrorMessage
        className="text-warning mt-2 italic text-right text-sm"
        name="text"
        component="div"
      />
    </div>
  );
}
export default TagTextInput;

import React from 'react';
import { Field, ErrorMessage } from 'formik';
import { minDays, maxDays } from '@/app/_utils/validation/validationVariables';
import UiText from '../../ui-library/UiText';

function LookBackPeriodInput({
  error,
  touched,
}: {
  error: string | undefined;
  touched: boolean | undefined;
}) {

  return (
    <div className="text-xs">
      <UiText className=" font-semibold py-2 mb-1">Lookback Period</UiText>
      <label hidden htmlFor="numberTag"></label>
      <div className="flex gap-2 items-center">
      <Field
        type="number"
        name="lookbackperiod"
        id="numberTag"
        min={minDays}
        max={maxDays}
        className={`border border-lightGrey p-2 rounded-lg focus:outline-none focus:border-darkGrey ${
          error && touched ? 'border-warning' : ''
        }`}
        placeholder="180"
      />
      <UiText className="">Days</UiText>
      </div>

      <ErrorMessage
        name="lookbackperiod"
        component="div"
        className="text-warning mt-2 italic text-right text-sm"
      />
    </div>
  );
}

export default LookBackPeriodInput;

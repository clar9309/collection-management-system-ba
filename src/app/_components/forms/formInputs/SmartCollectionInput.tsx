import React from 'react';
import { FieldProps, ErrorMessage, Field } from 'formik';
import { maxCollectionTitle, minCollectionTitle } from "@/app/_utils/validation/validationVariables";
import UiText from '../../ui-library/UiText';

function SmartCollectionInput( { error, touched }: { error: string | undefined; touched: boolean | undefined } & FieldProps) {
  return (
    <div className="text-sm">
      <UiText className="font-semibold py-2 mb-1">New Collection Title</UiText>
      <label hidden htmlFor="collectionName"></label>
      <Field
        type="text"
        id="collectionName"
        min={minCollectionTitle}
        max={maxCollectionTitle}
        className={`border border-lightGrey p-2 rounded-lg focus:outline-none focus:border-darkGrey ${
          error && touched ? 'border-warning' : ''
        }`}
        placeholder="Title for new Smart Collection to be generated"
      />
      <ErrorMessage
        name="collectionName"
        component="div"
        className="text-warning mt-2 italic text-right text-sm"
      />
    </div>
  );
}

export default SmartCollectionInput;

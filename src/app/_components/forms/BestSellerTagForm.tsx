import React from 'react';
import { Formik, Form, Field } from 'formik';
import TagNumberInput from './formInputs/TagNumberInput';
import TagTextInput from './formInputs/TagTextInput';
import createtagschema from '@/app/_utils/schemas/create-tag-schema';

function BestSellerTagForm() {
  return (
    <div className="text-xs">
      <Formik
        initialValues={{
          numberTag: '',
          textTag: ''
        }}
        validationSchema={createtagschema}
        onSubmit={(values, actions) => {
          console.log('Form Values:', values);
          actions.setSubmitting(false);
        }}
      >
        {({ errors, touched }) => (
          <Form>
            <Field name="textTag" component={TagTextInput} />
            <Field name="numberTag" component={TagNumberInput} />
          </Form>
        )}
      </Formik>
    </div>
  );
}

export default BestSellerTagForm;

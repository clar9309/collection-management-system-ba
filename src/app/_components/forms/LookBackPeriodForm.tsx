import React from 'react';
import { Formik, Form, Field } from 'formik';
import LookBackPeriodInput from './formInputs/LookBackPeriodInput';
import createLookBackSchema from '@/app/_utils/schemas/create-lookback-schema';

function LookBackPeriodForm() {
  return (
    <div className="text-xs">
      <Formik
        initialValues={{ numberTag: '' }}
        validationSchema={createLookBackSchema}
        onSubmit={(values, actions) => {
          console.log('Form Values:', values);
          actions.setSubmitting(false);
        }}
      >
        {({ errors, touched }) => (
          <Form>
            <Field
              name="numberTag"
              component={LookBackPeriodInput}
              errors={errors}
              touched={touched}
            />
            
          </Form>
        )}
      </Formik>
    </div>
  );
}

export default LookBackPeriodForm;

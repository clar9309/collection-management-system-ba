import React from 'react';
import CollectionIdPage from '@/app/(pages)/customize-collection/[collectionId]';

const CustomizeCollectionPage = () => {
  return <CollectionIdPage />;
};

export default CustomizeCollectionPage;

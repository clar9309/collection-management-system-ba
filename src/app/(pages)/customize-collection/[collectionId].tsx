"use client"
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/navigation';
import StoreUS from '../../data/store_us.json';
import StoreUK from '../../data/store_uk.json';
import CustomizeCollection from '@/app/_views/CustomizeCollection';

const CollectionIdPage = () => {
  const router = useRouter()
  const { collectionId } = (router as any).query || {};
  console.log(collectionId);

  const [collection, setCollection] = useState<null | undefined | {
    _id: string;
    name: string;
    description: string;
    category_id: string;
    enabled: boolean;
    type: string;
    products: {
      _id: string;
      name: string;
      price: number;
      color: string;
      sizes: string[];
    }[];
  }>(null);

  useEffect(() => {
    if (collectionId) {
      let fetchedCollection: typeof collection = null;
      if (StoreUK.collections.some(col => col._id === collectionId)) {
        fetchedCollection = StoreUK.collections.find(col => col._id === collectionId);
      } else if (StoreUS.collections.some(col => col._id === collectionId)) {
        fetchedCollection = StoreUS.collections.find(col => col._id === collectionId);
      }
      setCollection(fetchedCollection ?? null);
    }
  }, [collectionId]);

  return (
    <div>
      <CustomizeCollection />
    </div>
  );
};

export default CollectionIdPage;

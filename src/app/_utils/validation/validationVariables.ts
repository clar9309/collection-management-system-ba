// TAG //
export const minTagNumber = 1;
export const maxTagNumber = 200;

export const minTagText = 1; 
export const maxTagText = 200;

export const tagRegex = "/^[a-zA-Z0-9-_]+$/"; 

// LOOKBACK PERIOD //
export const minDays = 1;
export const maxDays = 365;

// SMART COLLECTION //
export const minCollectionTitle = 1;
export const maxCollectionTitle = 100;
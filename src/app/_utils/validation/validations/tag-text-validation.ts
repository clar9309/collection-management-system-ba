import { z } from "zod";
import { minTagText, maxTagText, tagRegex } from "../validationVariables";

export const tagTextValidation = z
    .string({
        required_error: "Text is required",
        invalid_type_error: "Text must be a string",
    })
    .transform(value => value.trim())
    .refine(value => tagRegex.test(value), { 
        message: "Text contains invalid characters. Only letters, numbers, dashes, and underscores are allowed.",
    })
    .refine(
        value => value.length >= minTagText && value.length <= maxTagText,
        {
            message: `Text must be between ${minTagText} and ${maxTagText} characters long.`,
        }
    );

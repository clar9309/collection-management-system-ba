import { z } from "zod";
import { minDays, maxDays } from "../validationVariables";

export const lookBackDaysValidation = z
  .number({
    required_error: "Days is required",
    invalid_type_error: "Must be a number",
  })
  .max(maxDays, {
    message: `Cannot be more than ${maxDays} days`,
  })
  .min(minDays, {
    message: `Must be at least ${minDays} days`,
  });

import { z } from "zod";
import { maxTagNumber, minTagNumber } from "../validationVariables";

export const tagNumberValidation = z
  .number({
    required_error: "Number is required",
    invalid_type_error: "Must be a number",
  })
  .max(maxTagNumber, {
    message: `Cannot tag more than ${maxTagNumber} bestsellers`,
  })
  .min(minTagNumber, {
    message: `Must tag at least ${minTagNumber} bestsellers`,
  });

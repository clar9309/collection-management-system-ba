import { z } from "zod";
import { maxCollectionTitle, minCollectionTitle } from "../validationVariables";

export const collectionTitleValidation = z
  .string({
    required_error: "Title is required",
    invalid_type_error: "Title must be a string",
  })
  .max(maxCollectionTitle, {
    message: `Title can not be longer than ${maxCollectionTitle} characthers`,
  })
  .min(minCollectionTitle, {
    message: `Ttile must be minimum ${minCollectionTitle} characthers long`,
  });

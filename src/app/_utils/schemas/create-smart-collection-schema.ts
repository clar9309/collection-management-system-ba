import { z } from "zod";
import { collectionTitleValidation } from "../validation/validations/smart-collection-validation";

const createsmartcollectionschema = z.object({
  collectionName: collectionTitleValidation,
});

export default createsmartcollectionschema;

import { z } from "zod";
import { lookBackDaysValidation } from "../validation/validations/lookback-days-validation";

const createlookbackschema = z.object({
  number: lookBackDaysValidation, 
});

export default createlookbackschema;

import { z } from "zod";
import { tagNumberValidation } from "../validation/validations/tag-number-validation";
import { tagTextValidation } from "../validation/validations/tag-text-validation";

const createtagschema = z.object({
  number: tagNumberValidation,
  text: tagTextValidation, 
});

export default createtagschema;
